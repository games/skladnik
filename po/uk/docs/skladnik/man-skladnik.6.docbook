<?xml version="1.0" ?>
<!DOCTYPE refentry PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN" "dtd/kdedbx45.dtd" [
<!ENTITY % Ukrainian "INCLUDE">
]>
<!--
SPDX-FileCopyrightText: Salvo Tomaselli <ltworf@debian.org>
SPDX-License-Identifier: GFDL-1.2-or-later
-->

<refentry lang="&language;">
<refentryinfo>
<title
>Сторінка підручника &skladnik;</title>
<date
>22 березня 2024 року</date>
<releaseinfo
>&skladnik; 0.5.2</releaseinfo>
</refentryinfo>

<refmeta>
<refentrytitle
><command
>skladnik</command
></refentrytitle>
<manvolnum
>6</manvolnum>
</refmeta>

<refnamediv>
<refname
><command
>skladnik</command
></refname>
<refpurpose
>Гра «Сокобан»</refpurpose>
</refnamediv>

<refsect1 id="description">
<title
>Опис</title>

<para
>Skladnik — реалізація японської гри у комірника, «сокобан».</para>

<para
>Ви граєте за комірника, якому треба перетягти скриньки на складі у відповідні їм місця. Проблема полягає у тому, що ви не можете тягнути скриньки або переступати через них. Якщо ви не будете обережними, деякі скриньки може бути заблоковано у невідповідних місцях і/або вони можуть заблокувати вам дорогу.</para>

</refsect1>


<refsect1 id="options">
<title
>Параметри</title>

<para
>&skladnik; налаштовується з графічної оболонки і не має жодних параметрів командного рядка, крім стандартних параметрів KDE Frameworks 6 і &Qt;.</para>

<variablelist>
<title
>Параметри &skladnik;</title>
<varlistentry>
<term
><option
>--help</option
></term>
<listitem>
<para
>Показати довідкові дані і завершити роботу.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--help-all</option
></term>
<listitem>
<para
>Вивести довідку, включно із специфічними для Qt параметрами.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--version</option
></term>
<listitem>
<para
>Вивести дані щодо версії і завершити роботу.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--author</option
></term>
<listitem>
<para
>Показати відомості щодо авторів програми.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--license</option
></term>
<listitem>
<para
>Показує інформацію щодо умов ліцензування програми.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--desktopfile</option
> <parameter
>НАЗВА_ФАЙЛА</parameter
></term>
<listitem>
<para
>Використати НАЗВУ_ФАЙЛА як базову назву файла для запису desktop цієї програми.</para>
</listitem>
</varlistentry>

</variablelist>

</refsect1>

<refsect1>
<title
>Додаткові відомості</title>

<simplelist>
<member
>З докладнішою документацією користувача можна ознайомитися за адресою <ulink url="help:/skladnik"
>help:/skladnik</ulink
> (або введіть цю адресу у браузері, або віддайте команду <userinput
><command
>khelpcenter</command
> <parameter
>help:/skladnik</parameter
></userinput
>).</member>
<member
><ulink
url="https://apps.kde.org/skladnik/"
>Домашня сторінка &skladnik;</ulink
></member>
<member
>kf6options(7)</member>
<member
>qt6options(7)</member>
</simplelist>

</refsect1>

<refsect1>
<title
>Автори</title>
<para
>Супровід &skladnik; здійснюється спільними зусиллями спільноти &kde;.</para>
<para
>Цю сторінку підручника приготовано <personname
><firstname
>Salvo</firstname
><surname
>Tomaselli</surname
></personname
> <email
>ltworf@debian.org</email
> для системи &Debian; &GNU;/&Linux; (але нею можна користуватися в інших системах).</para>
</refsect1>

</refentry>
